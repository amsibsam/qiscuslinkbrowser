package com.example.open.qiscusfilebrowser.ui.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.open.qiscusfilebrowser.R;
import com.example.open.qiscusfilebrowser.model.QiscusLink;

import java.util.List;

/**
 * Created by Rahardyan on 8/21/2015.
 */
public class LinkAdapter extends ArrayAdapter<QiscusLink> {
    private int resource;
    private LayoutInflater inflater;
    private QiscusLink link;
    private Holder holder;
    final Dialog settingsDialog = new Dialog(getContext());


    public LinkAdapter(Context context, int resource, List<QiscusLink> objects) {

        super(context, resource, objects);
        this.resource = resource;

        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        System.out.println("inflating asdsa");

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        System.out.println("///////////////////////////////////////// adapterfile");
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        link = getItem(position);

        if(convertView==null){
            holder = new Holder();

            convertView = inflater.inflate(resource, parent, false);
            System.out.println("inflating now");
            holder.textViewFileName = (TextView) convertView.findViewById(R.id.textViewFileNameItemLink);
            holder.textViewUploader = (TextView) convertView.findViewById(R.id.uploader);
            holder.textViewDate = (TextView) convertView.findViewById(R.id.textViewDate);
            holder.imageViewIconFile = (ImageView) convertView.findViewById(R.id.imagViewIconFile);
            holder.imageViewImagePopup = (ImageView) convertView.findViewById(R.id.imageViewPopup);

            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }

        System.out.println("nama file " + link.title);
        holder.textViewFileName.setText(link.title);
        holder.textViewUploader.setText(link.sender);
        holder.textViewDate.setText(link.url);

        holder.textViewFileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                link = getItem(position);
                Uri uri = Uri.parse(link.url); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                getContext().startActivity(intent);

            }
        });







        return convertView;
    }

    private static class Holder{
        TextView textViewFileName, textViewUploader, textViewDate, urlFile;
        ImageView imageViewIconFile, imageViewImagePopup;
        ImageButton close, download;

    }
}
