package com.example.open.qiscusfilebrowser.model;

import java.util.Date;

/**
 * Created by Rahardyan on 7/31/2015.
 */
public class QiscusLink {
    public final String title;
    public final String url;
    public final String sender;

    public QiscusLink(String title, String url, String sender) {
        this.title = title;
        this.url = url;
        this.sender = sender;
    }
}
