package com.example.open.qiscusfilebrowser.ui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.open.qiscusfilebrowser.R;
import com.example.open.qiscusfilebrowser.infrastructure.client.QiscusClient;
import com.example.open.qiscusfilebrowser.model.QiscusLink;
import com.example.open.qiscusfilebrowser.model.event.ClientCallback;
import com.example.open.qiscusfilebrowser.ui.adapter.LinkAdapter;

import java.util.List;

public class LinkActivity extends AppCompatActivity implements ClientCallback<QiscusLink> {
    private android.support.v7.widget.Toolbar toolbar;
    private ListView listViewLink;
    private LinkAdapter adapter;
    private QiscusLink link;
    private List<QiscusLink> links;
    private Dialog settingsDialog;
    private TextView urlFile;
    private ImageButton close;
    int idtopic;
    String namaTopic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file);

        Window w = getWindow();
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        listViewLink = (ListView) findViewById(R.id.listViewFile);
        close = (ImageButton) findViewById(R.id.imageButtonClose);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        settingsDialog = new Dialog(this);


        Bundle extras = getIntent().getExtras();
        idtopic = extras.getInt("idtopic");
        namaTopic = extras.getString("namaTopic");
        setTitle(namaTopic);
        System.out.println("idtopic di fileactivity " + idtopic);
        links = QiscusClient.getLinkTopic(idtopic, QiscusClient.token, LinkActivity.this);

        adapter = new LinkAdapter(this, R.layout.item_file, links);
        listViewLink.setAdapter(adapter);
        System.out.println("set adapter");

        listViewLink.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                link = links.get(position);
                Uri uri = Uri.parse(link.url); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });




    }



    @Override
    public void onSucceeded(QiscusLink result) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFailed() {

    }

    @Override
    public void onNoConnection() {

    }




}
